# Quick Summary #
The link to the source code samples contain the following:

1. Javascript - Custom event manager - This sample features dual event queues to prevent infinite loops caused by events spawning other events.

2. Objective-C - State Machine - I’ve used state machines like these to give in-game objects more intelligent and complex behaviors.
