//
//  MGState.h
//  MightyGuppy
//
//  Created by Paul De Leon on 2/23/12.
//  Copyright (c) 2012 Nexcra Software LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class MGTelegram;

@interface MGState : NSObject

- (void) enterWithEntity:(id)entityType;
- (void) executeWithEntity:(id)entityType deltaTime:(ccTime)deltaTime;
- (void) exitWithEntity:(id)entityType;

- (BOOL) onMessage:(MGTelegram*)telegram withEntity:(id)entityType;

@end
