//
//  BDMineOwnedStates.h
//  BigDigger
//
//  Created by Paul De Leon on 1/20/13.
//  Copyright (c) 2013 Nexcra Software LLC. All rights reserved.
//

#import "MGState.h"

@interface BDMineGlobalState : MGState
{
}

+ (BDMineGlobalState*) sharedState;
@end



@interface BDMineStartState : MGState
{
}

+ (BDMineStartState*) sharedState;
@end



@interface BDMineCountdownState : MGState
{
}

+ (BDMineCountdownState*) sharedState;
@end



@interface BDMineExplodeState : MGState
{
}

+ (BDMineExplodeState*) sharedState;
@end



@interface BDMineEndState : MGState
{
}

+ (BDMineEndState*) sharedState;
@end



#ifndef BD_MINE_OWNED_STATES_H
#define BD_MINE_OWNED_STATES_H

#define BD_CURRENT_MINE               (NXActor*) entityType
#define BD_CURRENT_MINE_COMP          (BDMineCompInterface*) [BD_CURRENT_MINE getComponent:kComponentMineID]
#define BD_CURRENT_MINE_STEERING      (NXASteeringCompInterface*) [BD_CURRENT_MINE getComponent:kComponentSteeringID]
#define BD_CURRENT_MINE_RENDER        (NXARenderCompInterface*) [BD_CURRENT_MINE getComponent:kComponentRenderID]
#define BD_CURRENT_MINE_FSM           [(NXAAICompInterface*) [BD_CURRENT_MINE getComponent:kComponentAIID] actorFsm]

#define MINE_GLOBAL_STATE			[BDMineGlobalState sharedState]
#define MINE_START_STATE			[BDMineStartState sharedState]
#define MINE_COUNTDOWN_STATE		[BDMineCountdownState sharedState]
#define MINE_EXPLODE_STATE			[BDMineExplodeState sharedState]
#define MINE_END_STATE              [BDMineEndState sharedState]

#endif
