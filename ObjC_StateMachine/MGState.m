//
//  MGState.m
//  MightyGuppy
//
//  Created by Paul De Leon on 2/23/12.
//  Copyright (c) 2012 Nexcra Software LLC. All rights reserved.
//

#import "MGState.h"

@implementation MGState


- (void) enterWithEntity:(id)entityType
{
    return;     // NOP
}


- (void) executeWithEntity:(id)entityType deltaTime:(ccTime)deltaTime
{
    return;     // NOP
}



- (void) exitWithEntity:(id)entityType
{
    return;     // NOP
}


- (BOOL) onMessage:(MGTelegram*)telegram withEntity:(id)entityType
{
    return false;   // NOP
}






@end
