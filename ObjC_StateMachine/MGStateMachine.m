//
//  MGStateMachine.m
//  MightyGuppy
//
//  Created by Paul De Leon on 2/23/12.
//  Copyright (c) 2012 Nexcra Software LLC. All rights reserved.
//

#import "MGTelegram.h"

#import "MGState.h"
#import "MGStateMachine.h"

@implementation MGStateMachine

@synthesize fsmOwner;

@synthesize currentState;
@synthesize previousState;
@synthesize globalState;




- (id) init
{
    self = [super init];
    
    if (self) 
    {
        fsmOwner = nil;
        
        currentState = nil;
        previousState = nil;
        globalState = nil;
    }
    
    return self;
}
    

- (id) initWithOwner:(id)owner startState:(MGState*)startState globalState:(MGState*)initGlobalState
{
    self = [self init];
    [self setFsmOwner:owner];
    [self changeState:startState];
    [self setGlobalState:initGlobalState];
    
    return self;
}


- (void) update:(ccTime)deltaTime
{
    if (globalState != nil) {
        [globalState executeWithEntity:fsmOwner deltaTime:deltaTime];
    }
    
    if (currentState != nil) {
        [currentState executeWithEntity:fsmOwner deltaTime:deltaTime];
    }
}


- (void) changeState:(MGState*)newState
{
    NSAssert(newState != nil, @"State Machine - trying to change to a nil state.");
    
    // Going to try using this so we don't have to manually check if we're changing the state in an external update function
    if (currentState != nil && [self isInState:newState]) {
        return;
    }
    
    previousState = currentState;
    [currentState exitWithEntity:fsmOwner];
    
    currentState = newState;
    [currentState enterWithEntity:fsmOwner];
}


- (void) revertToPreviousState
{
    [self changeState:previousState];
}


- (BOOL) isInState:(MGState*)stateToCheck
{
    NSAssert(currentState != nil, @"Tried to check current state of FSM when it's nil.");
    

    return ([currentState class] == [stateToCheck class]);
}


- (BOOL) handleMessage:(MGTelegram*)telegram
{
    BOOL messageHandled = NO;
    
    // Current State takes a crack
    messageHandled = [currentState onMessage:telegram withEntity:fsmOwner];
    
    // Then Global takes a crack
    if (messageHandled == NO) {
        messageHandled = [globalState onMessage:telegram withEntity:fsmOwner];
    }
    
    return messageHandled;
}


@end






@implementation NXProcessFsmChangeState

- (id) initWithFsm:(MGStateMachine*)inProcFsm state:(MGState*)inProcState
{
    self = [super init];
    
    if (self)
    {
        procFsm = inProcFsm;
        procState = inProcState;
    }
    
    return self;
}



- (void) onInit
{
    [super onInit];
}


- (void) onUpdate:(ccTime)delta
{
    [procFsm changeState:procState];
    [self succeed];
}


@end