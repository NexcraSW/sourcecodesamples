//
//  MGStateMachine.h
//  MightyGuppy
//
//  Created by Paul De Leon on 2/23/12.
//  Copyright (c) 2012 Nexcra Software LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class MGTelegram;
@class MGState;

@interface MGStateMachine : NSObject
{
    id __unsafe_unretained fsmOwner;
    
    MGState* currentState;
    MGState* previousState;
    MGState* globalState;
}
@property (nonatomic, unsafe_unretained) id fsmOwner;      // Weak pointer to the parent

@property (nonatomic, strong) MGState* currentState;
@property (nonatomic, strong) MGState* previousState;
@property (nonatomic, strong) MGState* globalState;

- (id) initWithOwner:(id)owner startState:(MGState*)startState globalState:(MGState*)initGlobalState;
- (void) update:(ccTime)deltaTime;

// Meant to be accessed by FSM Owned State classes
- (void) changeState:(MGState*)newState;
- (void) revertToPreviousState;

// Outside classes access these through the interface class
- (BOOL) isInState:(MGState*)stateToCheck;
- (BOOL) handleMessage:(MGTelegram*)telegram;

@end




@interface NXProcessFsmChangeState : NXProcess
{
    MGStateMachine* procFsm;
    MGState* procState;
}

- (id) initWithFsm:(MGStateMachine*)inProcFsm state:(MGState*)inProcState;
@end
