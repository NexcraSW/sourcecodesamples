//
//  BDMineOwnedStates.m
//  BigDigger
//
//  Created by Paul De Leon on 1/20/13.
//  Copyright (c) 2013 Nexcra Software LLC. All rights reserved.
//
//

#import "NXActor.h"

#import "NXASteeringCompInterface.h"
#import "NXARenderCompInterface.h"
#import "NXAAICompInterface.h"
#import "BDMineCompInterface.h"

#import "MGTelegram.h"
#import "MGStateMachine.h"

#import "BDMineOwnedStates.h"



//------------------------ BDMineGlobalState-------------------------------
@implementation BDMineGlobalState
static BDMineGlobalState* _sharedBDMineGlobalState = nil;

+ (BDMineGlobalState*) sharedState
{
	@synchronized([BDMineGlobalState class])
	{
		if (!_sharedBDMineGlobalState) {
			_sharedBDMineGlobalState = [[self alloc] init];
		}
		
		return _sharedBDMineGlobalState;
	}
}
@end



//------------------------ BDMineStartState-------------------------------
@implementation BDMineStartState
static BDMineStartState* _sharedBDMineStartState = nil;

+ (BDMineStartState*) sharedState
{
	@synchronized([BDMineStartState class])
	{
		if (!_sharedBDMineStartState) {
			_sharedBDMineStartState = [[self alloc] init];
		}
		
		return _sharedBDMineStartState;
	}
}


- (void) executeWithEntity:(id)entityType deltaTime:(ccTime)deltaTime
{
    [BD_CURRENT_MINE_FSM changeState:MINE_COUNTDOWN_STATE];
}
@end



//------------------------ BDMineCountdownState-------------------------------
@implementation BDMineCountdownState
static BDMineCountdownState* _sharedBDMineCountdownState = nil;

+ (BDMineCountdownState*) sharedState
{
	@synchronized([BDMineCountdownState class])
	{
		if (!_sharedBDMineCountdownState) {
			_sharedBDMineCountdownState = [[self alloc] init];
		}
		
		return _sharedBDMineCountdownState;
	}
}



- (void) executeWithEntity:(id)entityType deltaTime:(ccTime)deltaTime
{
    float newMineLiveDur = [BD_CURRENT_MINE_COMP mineLiveDur] - deltaTime;
    [BD_CURRENT_MINE_COMP setMineLiveDur:newMineLiveDur];
    
    if (newMineLiveDur < 0) {
        [BD_CURRENT_MINE_FSM changeState:MINE_EXPLODE_STATE];
    }
}


@end



//------------------------ BDMineExplodeState-------------------------------
@implementation BDMineExplodeState
static BDMineExplodeState* _sharedBDMineExplodeState = nil;

+ (BDMineExplodeState*) sharedState
{
	@synchronized([BDMineExplodeState class])
	{
		if (!_sharedBDMineExplodeState) {
			_sharedBDMineExplodeState = [[self alloc] init];
		}
		
		return _sharedBDMineExplodeState;
	}
}


- (void) enterWithEntity:(id)entityType
{
    [SHARED_EVENT_MANAGER queueEvent:[NXEventMineDetonated nxEventWithMine:BD_CURRENT_MINE sectionPos:[BD_CURRENT_MINE_COMP mineSectionPos]]];
}


- (void) executeWithEntity:(id)entityType deltaTime:(ccTime)deltaTime
{
    [BD_CURRENT_MINE_FSM changeState:MINE_END_STATE];
}

@end



//------------------------ BDMineEndState-------------------------------
@implementation BDMineEndState
static BDMineEndState* _sharedBDMineEndState = nil;

+ (BDMineEndState*) sharedState
{
	@synchronized([BDMineEndState class])
	{
		if (!_sharedBDMineEndState) {
			_sharedBDMineEndState = [[self alloc] init];
		}
		
		return _sharedBDMineEndState;
	}
}


- (void) executeWithEntity:(id)entityType deltaTime:(ccTime)deltaTime
{
    [SHARED_EVENT_MANAGER queueEvent:[NXEventActorRemoved nxEventWithActor:BD_CURRENT_MINE]];
}


@end



